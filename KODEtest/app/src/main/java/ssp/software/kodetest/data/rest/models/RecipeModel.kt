package ssp.software.kodetest.data.rest.models

import com.google.gson.annotations.SerializedName

data class RecipeModel(
    @SerializedName("recipes")
    var recipes:List<RecipeModelList>
)