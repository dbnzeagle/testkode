package ssp.software.kodetest.data.rest.datasource

import retrofit2.http.*
import rx.Observable
import ssp.software.kodetest.data.rest.models.DetailsModel
import ssp.software.kodetest.data.rest.models.RecipeModel
import ssp.software.kodetest.data.rest.models.RecipeModelDetails
import java.util.*


interface RecipesDataSource {

    @GET("/recipes")
    fun fetchRecipesList(): Observable<RecipeModel>

    @GET("/recipes/{uuid}")
    fun fetchDetails(@Path("uuid") uuid: String): Observable<DetailsModel>
}