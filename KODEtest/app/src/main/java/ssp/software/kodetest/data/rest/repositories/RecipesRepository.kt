package ssp.software.kodetest.data.rest.repositories

import android.content.Context
import rx.Observable
import ssp.software.kodetest.data.rest.datasource.RecipesDataSource
import ssp.software.kodetest.data.rest.models.DetailsModel
import ssp.software.kodetest.data.rest.models.RecipeModel
import ssp.software.kodetest.data.rest.models.RecipeModelDetails
import ssp.software.kodetest.data.rest.models.RecipeModelList
import javax.inject.Inject

class RecipesRepository @Inject constructor(private val recipesDataSource: RecipesDataSource, private val context: Context): Repository(){



    fun fetchRecipes(): Observable<RecipeModel> {
        return recipesDataSource
            .fetchRecipesList()
                .compose(this.applySchedulers<RecipeModel>())
    }
    fun fetchDetails(uuid:String): Observable<DetailsModel> {
        return recipesDataSource
                .fetchDetails(uuid)
                .compose(this.applySchedulers<DetailsModel>())
    }
}