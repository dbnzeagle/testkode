package ssp.software.kodetest.di.modules

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ssp.software.kodetest.Constants
import ssp.software.kodetest.data.rest.KodeInterceptor
import ssp.software.kodetest.data.rest.datasource.RecipesDataSource
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideHttpClient(recipesInterceptor: KodeInterceptor):OkHttpClient{
        val client = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(recipesInterceptor)
                .followRedirects(true)
                .build()

        return client
    }

    @Provides
    @Singleton
    fun provideKodeInterceptor(gson: Gson, context: Context): KodeInterceptor {
        return KodeInterceptor(gson, context)
    }

    @Provides
    @Singleton
    fun providesDefaultRetrofitAdapter(client: OkHttpClient, gson: Gson): Retrofit {
        val adapter = Retrofit.Builder()
                .baseUrl(Constants.V_API_MAIN)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        return adapter
    }

    @Provides
    fun provideRecipesSource(retrofit: Retrofit): RecipesDataSource {
        return retrofit.create(RecipesDataSource::class.java)
    }


}