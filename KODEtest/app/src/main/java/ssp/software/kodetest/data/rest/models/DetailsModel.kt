package ssp.software.kodetest.data.rest.models

import com.google.gson.annotations.SerializedName

data class DetailsModel(
    @SerializedName("recipe")
    var recipe:RecipeModelDetails
)