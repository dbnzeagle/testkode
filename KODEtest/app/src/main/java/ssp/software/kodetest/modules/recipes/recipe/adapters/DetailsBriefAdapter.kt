package ssp.software.kodetest.modules.recipes.recipe.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.brief_item.view.*
import ssp.software.kodetest.R
import ssp.software.kodetest.data.rest.models.RecipeModelBrief


class DetailsBriefAdapter(val briefData: List<RecipeModelBrief>) : RecyclerView.Adapter<DetailsBriefAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.brief_item, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(briefData[position])

    }


    override fun getItemCount(): Int {
        return briefData.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(recipe: RecipeModelBrief) {
            itemView.apply {
                brief_text.text=recipe.name
            }
        }

    }
}
