package ssp.software.kodetest.common.mvp


abstract class BasePresenter<V: IView>: IPresenter<V> {
    protected var view:V?=null

    constructor(view:V){
        this.view=view
    }

    override fun takeView(view: V){
        this.view=view
    }

    override fun onViewAttached(){

    }
    override fun setFilter(p:String){

    }

    override fun dropView(){
        view=null
        onViewAttached()

    }

    override fun onViewDetached(){}

}