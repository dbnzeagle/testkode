package ssp.software.kodetest.modules.recipes.recipe


import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import ssp.software.kodetest.di.AppComponent
import ssp.software.kodetest.di.scopes.FragmentScope


@FragmentScope
@Component(modules = [DetailsModule::class], dependencies = [AppComponent::class])
interface DetailsComponent {
    fun inject(detailsActivity: DetailsActivity)
}

@Module(includes = [DetailsModule.Declaration::class])
class DetailsModule(val detailsView: IDetailsView) {

    @Provides
    fun provideDetailsView(): IDetailsView {
        return detailsView
    }

    @Module
    interface Declaration {

        @Binds
        @FragmentScope
        fun bindDetailsPresenter(detailsPresenter: DetailsPresenter): IDetailsPresenter
    }
}