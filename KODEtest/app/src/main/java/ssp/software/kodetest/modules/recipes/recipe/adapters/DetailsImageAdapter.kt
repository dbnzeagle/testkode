package ssp.software.kodetest.modules.recipes.recipe.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.view.*
import kotlinx.android.synthetic.main.image_item.view.*
import org.jetbrains.anko.sdk25.listeners.onClick
import ssp.software.kodetest.R
import ssp.software.kodetest.data.rest.models.RecipeModelDetails


class DetailsImageAdapter(
     val data: List<String>,
     val context: Context,
     val onImageClick: ((String) -> Unit)? = null
) : PagerAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view === any as View
    }

    override fun getCount(): Int {
        return data.count()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.image_item, container, false)
        val currentImageItem = data[position]

        imageLayout.apply {
            Picasso.with(context).load(currentImageItem).resize(250, 250).into(images)
            images.onClick {
                onImageClick?.invoke(currentImageItem)
            }
        }

        container.addView(imageLayout)

        return imageLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}
