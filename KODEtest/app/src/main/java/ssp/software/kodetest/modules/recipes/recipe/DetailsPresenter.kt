package ssp.software.kodetest.modules.recipes.recipe

import rx.lang.kotlin.plusAssign
import ssp.software.kodetest.common.mvp.RxPresenter
import ssp.software.kodetest.data.rest.repositories.RecipesRepository
import javax.inject.Inject

class DetailsPresenter @Inject constructor(view: IDetailsView) : RxPresenter<IDetailsView>(view),
    IDetailsPresenter {
    @Inject
    lateinit var recipesRepository: RecipesRepository

    override fun onViewAttached() {
        super.onViewDetached()

    }

    override fun onImageClick(image: String) {
        view?.navigateToFullscreen(image)
    }

    override fun fetchDetail(uuid: String) {

        subscriptions += recipesRepository.fetchDetails(uuid)
            .subscribe(
                { details ->
                    view?.updateView(details = details.recipe)
                },
                { error ->
                }
            )
    }
}