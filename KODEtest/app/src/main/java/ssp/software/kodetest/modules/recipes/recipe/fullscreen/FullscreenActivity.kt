package ssp.software.kodetest.modules.recipes.recipe.fullscreen


import android.os.Bundle
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions.RxPermissions
import kotlinx.android.synthetic.main.activity_image.*
import org.jetbrains.anko.sdk25.listeners.onClick
import org.jetbrains.anko.toast
import ssp.software.kodetest.R
import ssp.software.kodetest.common.mvp.MvpActivity
import ssp.software.kodetest.di.AppComponent
import javax.inject.Inject

class FullscreenActivity:MvpActivity(),IFullscreenView{
    @Inject
    lateinit var presenter: FullscreenPresenter



    override fun resolveDependencies(appComponent: AppComponent) {
        DaggerFullscreenComponent.builder()
            .appComponent(appComponent)
            .fullscreenModule(FullscreenModule(this))
            .build()
            .inject(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        initViews()


    }
    private fun initViews() {
        val image=intent.getStringExtra("image")
        Picasso.with(this).load(image).into(fullscreen)
        floatingActionButton.onClick {
            RxPermissions.getInstance(this)
                .request(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe { granted ->
                    if (granted) {
                        presenter.downloadImage(image)
                    }
                }
             }
    }



    override fun beforeDestroy() {
        presenter.dropView()
    }

    override fun showInfo(message: String) {
        toast(message)
    }


}