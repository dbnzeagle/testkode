package ssp.software.kodetest.modules.recipes

import android.util.Log
import rx.Observable
import rx.lang.kotlin.plusAssign
import rx.lang.kotlin.toObservable
import ssp.software.kodetest.common.mvp.RxPresenter
import ssp.software.kodetest.data.rest.datasource.RecipesDataSource
import ssp.software.kodetest.data.rest.models.RecipeModelList
import ssp.software.kodetest.data.rest.repositories.RecipesRepository
import javax.inject.Inject

class RecipesPresenter @Inject constructor(view: IRecipesView) : RxPresenter<IRecipesView>(view), IRecipesPresenter {


    @Inject
    lateinit var recipesRepository: RecipesRepository

    override fun onViewAttached() {
        super.onViewAttached()
        fetchData()

    }
    override fun onSearchClick(query: String) {
            subscriptions+=recipesRepository.fetchRecipes()
                .subscribe(
                    {   recipeList ->
                        val lowerQuery = query.toLowerCase()
                        val searchedRecipesByName = recipeList.recipes.filter { recipe ->
                            return@filter recipe.name.toLowerCase().contains(lowerQuery)
                        }

                        val searchedRecipesByDescription = recipeList.recipes.filter { recipe ->
                            return@filter if (recipe.description == null) {
                                false
                            } else {
                                recipe.description.toLowerCase().contains(lowerQuery)
                            }
                        }

                        val searchedRecipesByInstructions = recipeList.recipes.filter { recipe ->
                            return@filter recipe.instructions.toLowerCase().contains(lowerQuery)
                        }

                        val resultSearchedRecipes = (searchedRecipesByName + searchedRecipesByInstructions + searchedRecipesByDescription).distinctBy { it.uuid }

                        view?.updateAdapter(resultSearchedRecipes)
                    },
                    {   error ->
                    }
                )

    }

    private fun fetchData() {
        subscriptions += recipesRepository.fetchRecipes()
            .subscribe(
                {   recipeList ->
                    view?.updateAdapter(recipesList = recipeList.recipes)
                },
                {   error ->
                }
            )
    }
    override fun onRecipeClick(recipe:RecipeModelList) {
        view?.navigateToDetails(recipe.uuid)
    }
}
