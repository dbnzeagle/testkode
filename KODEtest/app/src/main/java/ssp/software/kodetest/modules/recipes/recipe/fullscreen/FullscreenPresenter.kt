package ssp.software.kodetest.modules.recipes.recipe.fullscreen


import ssp.software.kodetest.common.mvp.RxPresenter
import javax.inject.Inject
import android.os.Environment
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.FileOutputStream
import java.net.URL


class FullscreenPresenter @Inject constructor(view: IFullscreenView) : RxPresenter<IFullscreenView>(view),
    IFullscreenPresenter {
    override fun downloadImage(url: String) {
        doAsync {fetch(url)
            uiThread{
                if (true)
                    view?.showInfo("Файл загружен")
                    else
                    view?.showInfo("Файл не загружен")
            }
        }


    }

    fun fetch(url: String):Boolean {
        val fileName = String.format("%d.jpg", System.currentTimeMillis())
        try {
            val url = URL(url)
            val input = url.openStream()
            try {
                val output = FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + fileName)
                val aReasonableSize = 1000
                val buffer = ByteArray(aReasonableSize)
                var bytesRead = input.read(buffer)
                try {
                    while (bytesRead  > 0) {
                        output.write(buffer, 0, bytesRead)
                        bytesRead = input.read(buffer)
                    }
                } finally {
                    output.close()
                }
            } finally {
                input.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }
}
