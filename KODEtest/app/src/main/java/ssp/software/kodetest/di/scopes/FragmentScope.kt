package ssp.software.kodetest.di.scopes

import javax.inject.Scope

@Scope
annotation class FragmentScope