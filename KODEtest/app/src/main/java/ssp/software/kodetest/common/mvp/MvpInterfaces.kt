package ssp.software.kodetest.common.mvp

import ssp.software.kodetest.KodeApplication


interface IView {
    fun application(): KodeApplication
    fun showMessage(message: String)
    fun showMessage(messageResId: Int)
}

interface IListView<M> : IView {
    fun showData(dataset: List<M>)
    fun notifyItemRemoved(item: M)
}

interface IPresenter<in V : IView> {
    fun setFilter(p:String)
    fun takeView(view: V)
    fun onViewAttached()
    fun dropView()
    fun onViewDetached()
}

interface IListPresenter<M, V : IListView<*>> : IPresenter<V> {
}

