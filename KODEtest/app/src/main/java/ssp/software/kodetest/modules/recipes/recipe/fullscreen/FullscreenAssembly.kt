package ssp.software.kodetest.modules.recipes.recipe.fullscreen


import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import ssp.software.kodetest.di.AppComponent
import ssp.software.kodetest.di.scopes.FragmentScope


@FragmentScope
@Component(modules = [FullscreenModule::class], dependencies = [AppComponent::class])
interface FullscreenComponent {
    fun inject(FullscreenFragment: FullscreenActivity)
}

@Module(includes = [FullscreenModule.Declarations::class])
class FullscreenModule(val fullscreenView: IFullscreenView) {

    @Provides
    fun provideFullscreenView(): IFullscreenView {
        return fullscreenView
    }

    @Module
    interface Declarations {

        @Binds
        @FragmentScope
        fun bindFullscreenPresenter(fullscreenPresenter: FullscreenPresenter): IFullscreenPresenter
    }
}
