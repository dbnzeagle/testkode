package ssp.software.kodetest.modules.recipes.recipe.fullscreen



import ssp.software.kodetest.common.mvp.IPresenter
import ssp.software.kodetest.common.mvp.IView


interface IFullscreenView : IView {
    fun showInfo(message:String)
}

interface IFullscreenPresenter : IPresenter<IFullscreenView> {
    fun downloadImage(url:String)
}
