package ssp.software.kodetest.common.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_reciepes.*
import org.jetbrains.anko.toast
import ssp.software.kodetest.KodeApplication
import ssp.software.kodetest.di.AppComponent
import ssp.software.kodetest.modules.recipes.RecipesActivity


abstract class BaseListActivity<M> : MvpActivity(), IListView<M> {
    protected var dataset: MutableList<M> = ArrayList()
    protected var adapter: RecyclerView.Adapter<*>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resolveDependencies(application().appComponent)




    }
    override fun showData(dataset: List<M>) {
        this.dataset.clear()
        this.dataset.addAll(dataset)
        adapter?.notifyDataSetChanged()
    }


    override fun notifyItemRemoved(item: M) {
        val index = dataset.indexOf(item)
        dataset.removeAt(index)
        adapter?.notifyItemRemoved(index)
    }


    override fun onDestroy() {
        beforeDestroy()
        super.onDestroy()
    }


    override fun application(): KodeApplication {
        return application as KodeApplication
    }



}
