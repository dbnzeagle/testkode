package ssp.software.kodetest.data.rest.models

import com.google.gson.annotations.SerializedName

data class RecipeModelList(
    @SerializedName("uuid")
    var uuid: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("images")
    var images: List<String>,
    @SerializedName("lastUpdated")
    var lastUpdated: Int,
    @SerializedName("description")
    var description: String,
    @SerializedName("instructions")
    var instructions: String,
    @SerializedName("difficulty")
    var difficulty: Int
)