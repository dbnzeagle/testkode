package ssp.software.kodetest.data.rest.models

import com.google.gson.annotations.SerializedName

data class RecipeModelBrief(
        @SerializedName("uuid")
        var uuid:String,
        @SerializedName("name")
        var name:String
)