package ssp.software.kodetest.modules.recipes.recipe

import ssp.software.kodetest.common.mvp.IPresenter
import ssp.software.kodetest.common.mvp.IView
import ssp.software.kodetest.data.rest.models.RecipeModelDetails


interface IDetailsView : IView {
    fun updateView(details: RecipeModelDetails)
    fun navigateToFullscreen(image: String)
}

interface IDetailsPresenter : IPresenter<IDetailsView> {
    fun fetchDetail(uuid: String)
    fun onImageClick(image: String)
}