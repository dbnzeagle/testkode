package ssp.software.kodetest.modules.recipes

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_reciepes.*
import org.jetbrains.anko.startActivity

import ssp.software.kodetest.R
import ssp.software.kodetest.common.mvp.BaseListActivity
import ssp.software.kodetest.data.rest.models.RecipeModelList
import ssp.software.kodetest.di.AppComponent
import ssp.software.kodetest.modules.recipes.recipe.DetailsActivity
import javax.inject.Inject

import android.view.Menu
import android.view.MenuItem


class RecipesActivity : BaseListActivity<RecipeModelList>(), IRecipesView {


    @Inject
    lateinit var presenter: RecipesPresenter
    override fun navigateToDetails(uuid: String?) {
        startActivity<DetailsActivity>("uuid" to uuid)
    }

    override fun resolveDependencies(appComponent: AppComponent) {
        DaggerRecipesListComponent.builder()
            .appComponent(appComponent)
            .recipesListModule(RecipesListModule(this))
            .build()
            .inject(this)
    }

    override fun updateAdapter(recipesList: List<RecipeModelList>) {
        val defString: MutableList<String?> = mutableListOf()
        var adapter: RecipesAdapter? = null
        adapter = RecipesAdapter(
            dataList = recipesList,
            onItemClick = { recipeModelList -> presenter.onRecipeClick(recipeModelList) }
        )
        defString.add("Выберите сортировку")
        defString.add("Сортировка названию")
        defString.add("Сортировка по дате обновления")
        val mySpinner = spinner
        mySpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, defString)
        mySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                recyclerView.adapter = adapter
            }

            override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                if (position == 1) {
                    val sorted = recipesList.sortedWith(compareBy { it.name })
                    adapter = RecipesAdapter(
                        dataList = sorted,
                        onItemClick = { recipeModelList -> presenter.onRecipeClick(recipeModelList) }

                    )
                    recyclerView.adapter = adapter
                } else if (position == 2) {
                    val sorted = recipesList.sortedWith(compareBy { it.lastUpdated })
                    adapter = RecipesAdapter(
                        dataList = sorted,
                        onItemClick = { recipeModelList -> presenter.onRecipeClick(recipeModelList) }

                    )
                    recyclerView.adapter = adapter
                } else {
                    adapter = RecipesAdapter(
                        dataList = recipesList,
                        onItemClick = { recipeModelList -> presenter.onRecipeClick(recipeModelList) }

                    )
                    recyclerView.adapter = adapter
                }
            }

        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciepes)

        initViews()
        presenter.onViewAttached()

    }

    private fun initViews() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

    }


    override fun beforeDestroy() {
        presenter.dropView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(ssp.software.kodetest.R.menu.menu_main, menu)

        val search = menu.findItem(R.id.search)
        val searchView = search.actionView as SearchView
        search(searchView)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return super.onOptionsItemSelected(item)
    }

    private fun search(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                presenter.onSearchClick(newText
                )
                return true
            }
        })
    }

}
