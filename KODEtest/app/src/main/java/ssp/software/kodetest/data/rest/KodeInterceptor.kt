package ssp.software.kodetest.data.rest

import android.content.Context
import android.util.Log
import okhttp3.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.io.IOException
import java.net.SocketTimeoutException


class KodeInterceptor(val gson: Gson, val context: Context) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val request: Request = chain.request()
                .newBuilder()
                .addHeader("Content-Type", "application/json")
                .build()

            Log.d("URL", request.url().toString())
            val result = chain.proceed(request)
            return result
        } catch (e: SocketTimeoutException) {
            throw IOException("Не удалось установить соединение с сервером", e)
        }
    }

}
