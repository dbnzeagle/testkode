package ssp.software.kodetest.modules.recipes.recipe

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import ssp.software.kodetest.R
import ssp.software.kodetest.common.mvp.MvpActivity
import ssp.software.kodetest.data.rest.models.RecipeModelDetails
import ssp.software.kodetest.di.AppComponent
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_details.*
import ssp.software.kodetest.modules.recipes.recipe.adapters.DetailsBriefAdapter
import ssp.software.kodetest.modules.recipes.recipe.adapters.DetailsImageAdapter
import ssp.software.kodetest.modules.recipes.recipe.fullscreen.FullscreenActivity
import org.jetbrains.anko.startActivity


class DetailsActivity : MvpActivity(), IDetailsView {


    @Inject
    lateinit var presenter: DetailsPresenter

    override fun resolveDependencies(appComponent: AppComponent) {
        DaggerDetailsComponent.builder()
            .appComponent(appComponent)
            .detailsModule(DetailsModule(this))
            .build()
            .inject(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        initViews()


    }

    private fun initViews() {
        presenter.fetchDetail(intent.getStringExtra("uuid"))
        briefList.layoutManager = LinearLayoutManager(this)
        briefList.setHasFixedSize(true)
    }

    override fun updateView(details: RecipeModelDetails) {
        val imageAdapter = DetailsImageAdapter(
            data = details.images,
            context = this,
            onImageClick = { image -> presenter.onImageClick(image) }
        )
        val briefAdapter = DetailsBriefAdapter(
            briefData = details.similar
        )
        briefList.adapter = briefAdapter
        imageView.adapter = imageAdapter
        name_details.text = details.name
        description.text = details.description
        instructions.text = details.instructions.replace("<br>", "\n")
        ratingBar.rating = details.difficulty.toFloat()

    }

    override fun navigateToFullscreen(image: String) {
        startActivity<FullscreenActivity>("image" to image)
    }

    override fun beforeDestroy() {
        presenter.dropView()
    }


}