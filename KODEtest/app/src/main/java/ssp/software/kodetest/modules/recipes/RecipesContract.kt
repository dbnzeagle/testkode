package ssp.software.kodetest.modules.recipes

import ssp.software.kodetest.common.mvp.IListPresenter
import ssp.software.kodetest.common.mvp.IListView

import ssp.software.kodetest.data.rest.models.RecipeModelList

interface IRecipesView : IListView<RecipeModelList> {
    fun navigateToDetails(uuid: String?)
    fun updateAdapter(recipesList: List<RecipeModelList>)
}

interface IRecipesPresenter : IListPresenter<RecipeModelList, IRecipesView> {
    fun onRecipeClick(recipe: RecipeModelList)
    fun onSearchClick(query:String)
}