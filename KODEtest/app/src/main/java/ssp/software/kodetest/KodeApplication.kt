package ssp.software.kodetest

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import ssp.software.kodetest.di.AppComponent
import ssp.software.kodetest.di.DaggerAppComponent
import ssp.software.kodetest.di.modules.AppModule


class KodeApplication : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()


    }


}