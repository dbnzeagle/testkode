package ssp.software.kodetest.common.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.toast
import ssp.software.kodetest.KodeApplication
import ssp.software.kodetest.di.AppComponent


abstract class MvpActivity : AppCompatActivity(), IView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resolveDependencies(application().appComponent)

    }

    override fun onDestroy() {
        beforeDestroy()
        super.onDestroy()
    }

    abstract fun resolveDependencies(appComponent: AppComponent)

    abstract fun beforeDestroy()

    override fun application(): KodeApplication {
        return application as KodeApplication
    }

    override fun showMessage(message: String) {
        toast(message)
    }


    override fun showMessage(messageResId: Int) {
        toast(messageResId)
    }
}