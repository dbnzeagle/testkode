package ssp.software.kodetest.modules.recipes

import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import ssp.software.kodetest.di.AppComponent
import ssp.software.kodetest.di.scopes.ActivityScope


@ActivityScope
@Component(modules = arrayOf(RecipesListModule::class), dependencies = arrayOf(AppComponent::class))
interface RecipesListComponent {
    fun inject(recipesActivity: RecipesActivity)
}


@Module(includes = arrayOf(RecipesListModule.Declarations::class))
class RecipesListModule(val recipesListView: IRecipesView) {

    @Provides
    fun provideRecipesView(): IRecipesView {
        return recipesListView
    }

    @Module
    interface Declarations {
        @Binds
        @ActivityScope
        fun bindRecipesListPresenter(recipesListPresenter: RecipesPresenter): IRecipesPresenter
    }
}