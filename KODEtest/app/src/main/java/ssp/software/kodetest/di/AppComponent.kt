package ssp.software.kodetest.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import dagger.Component
import okhttp3.OkHttpClient
import ssp.software.kodetest.data.rest.KodeInterceptor
import ssp.software.kodetest.data.rest.datasource.RecipesDataSource
import ssp.software.kodetest.di.modules.AppModule
import ssp.software.kodetest.di.modules.NetworkModule
import ssp.software.kodetest.di.modules.PushNotificationModule
import ssp.software.kodetest.modules.recipes.recipe.DetailsActivity

import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, PushNotificationModule::class])
interface AppComponent {
    fun provideApplication(): Application
    fun provideGson(): Gson
    fun provideContext(): Context
    fun provideDetailsFragment(): DetailsActivity

    fun provideRecipesDataSource(): RecipesDataSource

    fun provideHttpClient(): OkHttpClient

    fun provideRecipesInterceptor(): KodeInterceptor

}