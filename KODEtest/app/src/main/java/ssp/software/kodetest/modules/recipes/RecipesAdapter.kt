package ssp.software.kodetest.modules.recipes

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycler_item.view.*
import org.jetbrains.anko.sdk25.listeners.onClick
import ssp.software.kodetest.R
import ssp.software.kodetest.data.rest.models.RecipeModelList




class RecipesAdapter(val dataList:
                     List<RecipeModelList>, val onItemClick: ((RecipeModelList) -> Unit)? = null) :
    RecyclerView.Adapter<RecipesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item, parent, false)
        return ViewHolder(view, onItemClick)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position])

    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View, val onItemClick: ((RecipeModelList) -> Unit)? = null) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(task: RecipeModelList) {
            itemView.apply {
                recipe.text = task.name
                name.text = task.description
                Picasso.with(context).load(task.images.first()).resize(300, 300).into(imageRecipes)
                recipes_card.onClick { onItemClick?.invoke(task) }

            }
        }

    }
}
