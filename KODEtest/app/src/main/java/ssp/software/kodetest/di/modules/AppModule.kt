package ssp.software.kodetest.di.modules

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ssp.software.kodetest.KodeApplication
import ssp.software.kodetest.modules.recipes.recipe.DetailsActivity
import javax.inject.Singleton

@Module
class AppModule(val application: KodeApplication) {

    @Provides
    fun provideApplication():Application{
        return application
    }



    @Provides
    @Singleton
    fun provideGson():Gson{
        val gson=GsonBuilder().setLenient().create()
    return gson
    }


    @Provides
    @Singleton
    fun provideContext():Context{
        return application.applicationContext
    }

    @Provides
    fun provideDetailsFragment(): DetailsActivity {
        return DetailsActivity()
    }


}